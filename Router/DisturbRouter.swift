//
//  DisturbRouter.swift
//  DobroDocPlus
//
//  Created by Anton Vovk on 07.02.2020.
//  Copyright © 2020 Dmytro Demchenko. All rights reserved.
//

import Foundation
import UIKit

protocol DisturbRouter: UIViewControllerPresentation {
    func showConsultationBooking(medicalCard: MedicalCard?, specialization: DoctorSpecialization)
}

extension DisturbViewController: DisturbRouter, BaseView {
    func showConsultationBooking(medicalCard: MedicalCard?, specialization: DoctorSpecialization) {
        let bookingModel = ConsultationBookingModel(medicalCard: medicalCard,
                                                    doctorSpecialization: specialization)
        push(module: ConsultationBookingModule(bookingModel: bookingModel))
    }
}
