//
//  DisturbData.swift
//  DobroDocPlus
//
//  Created by Anton Vovk on 07.02.2020.
//  Copyright © 2020 Dmytro Demchenko. All rights reserved.
//

import Foundation

protocol DisturbDataDelegate {
    func didGetError(message: String?)
    func didGetSymptoms(symptoms: [SymptomResponse])
}

final class DisturbData {
    // MARK: - Properties
    private var delegate: DisturbDataDelegate?
    
    // MARK: - Init
    init(delegate: DisturbDataDelegate) {
        self.delegate = delegate
    }
    
    // MARK: - Search
    func searchForSpecialtyBy(token: String, symptom: String) {
        let searchRequest = SymptomsSearchRequest(accessToken: token,
                                                  searchString: symptom)
        NetworkManager.shared().perform(request: searchRequest) { [weak self] (dict, data, networkError) in
            // Checking for errors
            if let networkError = networkError {
                let message = networkError.errors ?? networkError.message
                self?.delegate?.didGetError(message: message)
                return
            }
            // Unwrapping data
            guard let dataObj = data else {
                self?.delegate?.didGetError(message: "Data object is missing")
                return
            }
            // Decoding response
            do {
                let searchResponse: SymptomsSearchResponse = try JSONDecoder().decode(SymptomsSearchResponse.self, from: dataObj)
                self?.delegate?.didGetSymptoms(symptoms: searchResponse.data.items)
            } catch {
                self?.delegate?.didGetError(message: error.localizedDescription)
            }
        }
    }
}
