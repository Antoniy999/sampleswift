//
//  DisturbPresenter.swift
//  DobroDocPlus
//
//  Created by Anton Vovk on 07.02.2020.
//  Copyright © 2020 Dmytro Demchenko. All rights reserved.
//

import Foundation

protocol DisturbPresenterProtocol: class {
    func neededToSearchForSymptom(searchString: String)
}

final class DisturbPresenter {
    // MARK: - Properties
    private weak var view: DisturbViewControllerProtocol!
    private weak var router: DisturbRouter?
    private(set) var data: DisturbData?
    private var keychain: UserKeychainAccessible!
    
    // MARK: - Init
    init(with view: DisturbViewControllerProtocol, router: DisturbRouter) {
        self.view = view
        self.router = router
        self.data = DisturbData(delegate: self)
        self.keychain = UserKeychainAccess(localQuery: LocalKeychainQueryProvider())
    }
}

// MARK: - DisturbDataDelegate
extension DisturbPresenter: DisturbDataDelegate {
    func didGetError(message: String?) {
        view.didGetError(message: message)
    }
    
    func didGetSymptoms(symptoms: [SymptomResponse]) {
        var specialties: [Specialty] = []
        symptoms.forEach({ specialties.append(contentsOf: $0.specialty) })
        view.didGetSpecialties(specialties: specialties)
    }
}

// MARK: - Private
private extension DisturbPresenter {
    func getAccessToken() -> String? {
        do {
            return try keychain.getAccessToken()
        } catch {
            return nil
        }
    }
}

// MARK: - DisturbPresenterProtocol
extension DisturbPresenter: DisturbPresenterProtocol {
    func neededToSearchForSymptom(searchString: String) {
        guard let token = getAccessToken() else { return }
        data?.searchForSpecialtyBy(token: token, symptom: searchString)
    }
}
