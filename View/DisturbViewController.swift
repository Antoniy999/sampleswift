//
//  DisturbViewController.swift
//  DobroDocPlus
//
//  Created by Anton Vovk on 07.02.2020.
//  Copyright © 2020 Dmytro Demchenko. All rights reserved.
//

import UIKit

protocol DisturbViewControllerProtocol: class {
    func didGetError(message: String?)
    func didGetSpecialties(specialties: [Specialty])
}

class DisturbViewController: UIViewController {
    // MARK: - Properties
    var presenter: DisturbPresenterProtocol!
    private var frequentRequestsViewMaxHeight: CGFloat = 300.0
    private var cellSpacing: CGFloat = 20.0
    private var numberOfItemsInRow: Int = 2
    private var dataSource: [Specialty] = [] {
        didSet {
            itemsCollectionView.reloadData()
        }
    }

    // MARK: - Outlets
    @IBOutlet private weak var searchFieldContainer: UIView!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var frequentRequestsView: FrequentRequestsView!
    @IBOutlet private weak var itemsCollectionView: UICollectionView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet private weak var frequentRequestsViewHeight: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBaseAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - Private
private extension DisturbViewController {
    func setupBaseAppearance() {
        titleLabel.text = R.string.localizable.whatIsYourDisturbing()
        setupFrequentRequestsView()
        setupSearchTextField()
        setupCollectionView()
    }
    
    func setupSearchTextField() {
        searchTextField.delegate = self
        searchTextField.leftViewMode = .always
        searchTextField.textColor = Palette.searchPlaceholderColor
        searchFieldContainer.layer.cornerRadius = 8
        // Placeholder
        searchTextField.placeholder = R.string.localizable.whatIsYourDisturbing()
        // Clear button
        searchTextField.clearButtonMode = .whileEditing
        if let clearButton = searchTextField.value(forKeyPath: "_clearButton") as? UIButton {
            clearButton.setImage(UIImage(named: "deleteButton"), for: .normal)
            clearButton.setImage(UIImage(named: "deleteButton"), for: .highlighted)
        }
    }
    
    func setupFrequentRequestsView() {
        frequentRequestsView.layer.cornerRadius = 8
        frequentRequestsView.setupDataSource(dataSource: generateFrequentRequestsList())
        frequentRequestsView.setupDelegate(delegate: self)
    }
    
    func animateRequestView() {
        let currentHeight = frequentRequestsViewHeight.constant
        let maxViewHeight = frequentRequestsView.getViewHeight(maxHeight: frequentRequestsViewMaxHeight)
        frequentRequestsViewHeight.constant = currentHeight == 0 ? maxViewHeight : 0.0
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
        }
    }
    
    func setupCollectionView() {
        let cellNib = UINib(nibName: DoctorTypeCollectionViewCell.identifier, bundle: nil)
        itemsCollectionView.register(cellNib,
                                     forCellWithReuseIdentifier: DoctorTypeCollectionViewCell.identifier)
        itemsCollectionView.delegate = self
        itemsCollectionView.dataSource = self
    }
    
    func searchForSymptom(searchString: String) {
        ProgressHud.show()
        presenter.neededToSearchForSymptom(searchString: searchString)
    }
    
    #warning("temporary")
    func generateFrequentRequestsList() -> [String] {
        return ["Боль в горле", "Одышка",
                "Кружится голова", "Депрессия"]
    }
}

// MARK: - DisturbViewControllerProtocol
extension DisturbViewController: DisturbViewControllerProtocol {
    func didGetError(message: String?) {
        ProgressHud.dismiss()
        presentError(message: message)
    }
    
    func didGetSpecialties(specialties: [Specialty]) {
        ProgressHud.dismiss()
        self.dataSource = specialties
        self.itemsCollectionView.isHidden = specialties.count == 0
    }
}

// MARK: - UITextFieldDelegate
extension DisturbViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateRequestView()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let searchString = textField.text {
            searchForSymptom(searchString: searchString)
        }
        animateRequestView()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let searchString = textField.text {
            searchForSymptom(searchString: searchString)
        }
        animateRequestView()
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - UICollectionViewDelegate
extension DisturbViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let specialization = dataSource[indexPath.item].getSpecialization()
        let medicalCard: MedicalCard? = UserCardStorage.shared.getCard()
        showConsultationBooking(medicalCard: medicalCard, specialization: specialization)
    }
}

// MARK: - UICollectionViewDataSource
extension DisturbViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: DoctorTypeCollectionViewCell.identifier,
            for: indexPath) as? DoctorTypeCollectionViewCell else {
            fatalError("Error: no such cell with id \(DoctorTypeCollectionViewCell.identifier)")
        }
        let item = dataSource[indexPath.item]
        cell.setWith(name: item.title, nameImage: item.imageUrl ?? "")
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension DisturbViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalSpacingWidth = CGFloat(numberOfItemsInRow - 1) * cellSpacing
        let cellWidth: CGFloat = (collectionView.frame.width - totalSpacingWidth) / CGFloat(numberOfItemsInRow)
        return CGSize(width: cellWidth, height: cellWidth * 0.7)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
}

// MARK: - FrequentRequestsViewDelegate
extension DisturbViewController: FrequentRequestsViewDelegate {
    func didSelectRequest(with title: String) {
        searchTextField.text = title
        searchTextField.resignFirstResponder()
    }
}
