//
//  DisturbModule.swift
//  DobroDocPlus
//
//  Created by Anton Vovk on 09.02.2020.
//  Copyright © 2020 Dmytro Demchenko. All rights reserved.
//

import Foundation
import UIKit

final class DisturbModule: ModuleInitializable {
    // MARK: - Properties
    private let view: DisturbViewController
    private let presenter: DisturbPresenter
    
    // MARK: - Init
    init() {
        view = R.storyboard.disturb.disturbViewController()!
        presenter = DisturbPresenter(with: view, router: view)
        view.presenter = presenter
    }
    
    // MARK: - Public
    func viewController() -> UIViewController {
        return view
    }
}
